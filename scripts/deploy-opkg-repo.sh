#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# Validates if the IPK version and git tag agree.  The tag must contain the 
# version
# Globals:
#   CI_COMMIT_TAG
#   ORACLE_SID
# Arguments:
#   None
################################################################################
function validate_tag_version(){
	ipk_version='v'$(grep "^Version: " ipk/control/control | cut -d' ' -f2-)

	if [[ "$CI_COMMIT_TAG" == *"$ipk_version"* ]]; then
		echo "--> git tag and IPK version agree"
	else
		echo "Error: git tag does not mach IPK version in ipk/control/control"
		exit 1
	fi
}

################################################################################
# Copies all IPKs in the current directory to the package repo
# version
# Globals:
#   GCLOUD_SERVICE_KEY
#   GCLOUD_COMPUTE_INSTANCE
#   GCLOUD_PROJECT
# Arguments:
#   channel - should be 'stable' or 'dev'
################################################################################
function deploy_to_gcp(){
	channel=$1
	gcloud auth activate-service-account --key-file=$GCLOUD_SERVICE_KEY

	VERSION=$(cat ipk/control/control | grep "Version" | cut -d' ' -f 2)
	PACKAGE=$(cat ipk/control/control | grep "Package" | cut -d' ' -f 2)
	IPK_NAME=${PACKAGE}_${VERSION}.ipk

	# for dev channel, stick a timestamp in the IPK name
	if [[ "$OPKG_CHANNEL" == "dev" ]]; then
		pkg_name=${IPK_NAME%.*}
		dts=$(date +"%Y%m%d%H%M")
		ipk=$pkg_name'_'$dts'.ipk'
		mv $IPK_NAME $ipk
	fi
	echo "--> deploying $ipk to: $GCLOUD_COMPUTE_INSTANCE:/opkg/"$channel
	gcloud --project $GCLOUD_PROJECT compute scp $"$ipk" $GCLOUD_COMPUTE_INSTANCE:/opkg/$channel --zone us-west2-a
	
	# prevent gcp instance SSH key limit from getting hit
	for i in $(gcloud compute os-login ssh-keys list); 
	do 
		if [ "$i" != "FINGERPRINT" ] && [ "$i" != "EXPIRY" ]; then
			echo "--> removing key:"
			gcloud compute os-login ssh-keys remove --key $i;
		fi
	done
}

# exit on error
set -e

WORK_DIR=${PWD}

echo "--> running from: " $WORK_DIR
echo "--> using channel: /opkg/" $OPKG_CHANNEL

# prevent no ipks generating a blank .ipk found
shopt -s nullglob
if [[ -n $(echo *.ipk) ]]
then
	if [[ "$OPKG_CHANNEL" == "stable" ]]; then
		validate_tag_version
	fi
	deploy_to_gcp $OPKG_CHANNEL
else
	echo "Error: no IPKs found.  Please check build stage"
	exit 1
fi
